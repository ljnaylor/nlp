# README #

*A brief overview of NLP/Classification/RNN functionality in Pytorch

*Dependencies: torch, numpy, matplotlib, python 3.6.5



### What is this repository for? ###

* Quick summary of character level recurrent neural networks, guesses the nationality of names

* 0.1

### How do I get set up? ###

* Install above dependencies if necessary
* Database configuration: run wget 'https://download.pytorch.org/tutorial/data.zip' in terminal or download to working directory (windows may require alteration)
* Run the script charlevel_rnn.py
* Predictions can be made via adding a predict function with a first name string e.g. predict('Joe')

